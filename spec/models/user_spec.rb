require 'rails_helper'

RSpec.describe User, type: :model do

  # associations
  it { is_expected.to have_one(:membership).dependent(:destroy) }

  # columns
  it { is_expected.to have_db_column(:email).of_type(:string) }
  it { is_expected.to have_db_column(:admin).of_type(:integer) }

  # methods
  describe "#is_admin" do
    let(:user_admin) { FactoryBot.create(:user,
                                         :email => "naomi@dakota.me",
                                         :password => "whola^^$11",
                                         :admin => 1) }
    let(:user_regular) { FactoryBot.create(:user,
                                           :email => "grace@dakota.me",
                                           :password => "aaola^^$11") }
    context "when the user is an admin" do
      it "should return true" do
        expect(user_admin.is_admin).to be true
      end
    end

    context "when the user is not an admin" do
      it "should return false" do
        expect(user_regular.is_admin).to be false
      end
    end
  end

  describe "#admin_status_in_words" do
    let(:user_admin) { FactoryBot.create(:user,
                                         :email => "naomi@dakota.me",
                                         :password => "whola^^$11",
                                         :admin => 1) }
    let(:user_regular) { FactoryBot.create(:user,
                                           :email => "grace@dakota.me",
                                           :password => "aaola^^$11") }
    context "when the user is an admin" do
      it "should return string Admin" do
        user_admin.admin_status_in_words == "Admin"
      end
    end

    context "when the user is not an admin" do
      it "should return string Normal user" do
        expect(user_regular.admin_status_in_words).to eq "Normal user"
      end
    end
  end

  describe "#has_premium_membership" do
    let(:user) { FactoryBot.create(:user,
                                   :email => "tony@dakota.me",
                                   :password => "55va^^$11") }

    context "when the user does not have a premium membership" do
      it "should return false" do
        expect(user.has_premium_membership).to be false
      end
    end

    context "when the user has a premium membership" do
      it "should return true" do
        FactoryBot.create(:membership,
                          :user => user)
        expect(user.has_premium_membership).to be true
      end
    end

  end

end
