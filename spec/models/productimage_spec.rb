require 'rails_helper'

RSpec.describe Productimage, type: :model do
  before {
    @product = FactoryBot.create(:product,
                                 :name => "iMac G3",
                                 :price => 30000,
                                 :discount_price => 29999)

    @image = FactoryBot.create(
      :productimage,
      :product => @product,
      :productpicture => Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/g3_2.jpg'), 'image/jpeg')
    )
  }


  # associations
  it { is_expected.to belong_to(:product) }

  # columns
  it { is_expected.to have_db_column(:product_id).of_type(:integer) }
  it { is_expected.to have_db_column(:productpicture).of_type(:string) }


  it "is valid with a productpicture" do
    expect(@image).to be_valid
  end

  it "is not valid without a productpicture" do
    @image.productpicture = nil
    expect(@image).to_not be_valid
  end

end
