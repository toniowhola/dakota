require 'rails_helper'

RSpec.describe Membership, type: :model do

  # associations
  it { is_expected.to belong_to(:user) }

  # columns 
  it { is_expected.to have_db_column(:user_id).of_type(:integer) }

end
