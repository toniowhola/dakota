require 'rails_helper'

RSpec.describe Product, type: :model do

  # associations
  it { is_expected.to have_many(:productimages).dependent(:destroy) }

  # columns 
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:price).of_type(:integer) }
  it { is_expected.to have_db_column(:discount_price).of_type(:integer) }

  # methods
  describe "#cover_image" do
    before(:each) do
      @product = FactoryBot.create(:product,
                                   :name => "BMW M3",
                                   :price => 23999,
                                   :discount_price => 19000)
    end

    context "when the product does not have any image" do
      it "should return the placeholder image" do
        expect(@product.cover_image).to eq "/images/placeholder.jpg"
      end
    end

    context "when the product has images" do
      it "should return the product image" do
        @productimage = FactoryBot.create(
          :productimage,
          :product_id => @product.id,
          :productpicture => Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/g3_2.jpg'), 'image/jpeg')
        )
        expect(@product.cover_image).to match(/g3_2/)
      end
    end
  end

end
