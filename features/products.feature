Feature: Products

  A visitor should be able to view all the products
  Each product should have a name, price and images

  Background:
    Given I am a registered user
    And I am logged in

  Scenario: Listing products
    Given I have populated the data for several products
    When I visit the webpage
    Then I should see the list of products

  Scenario: Product page for regular user
    Given A burger is in the product listing
    When I am on the burger page
    Then I should see the product name for the burger
    And I should see the price for the burger
    And I should not see the discount price for the burger
