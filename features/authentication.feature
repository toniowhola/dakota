Feature: Authentication

  A user can register as a normal user, log in and log out

  Scenario: Signing up
      Given I visit the webpage
      When I fill in the sign up form
      And I confirm the email
      Then I should see that my account is confirmed

  Scenario: Logs In
      Given I am a registered user
      And I visit the webpage
      When I fill in the login form
      Then I should be logged in

  Scenario: Logs Out
      Given I am a registered user
      And I am logged in
      And I visit the webpage
      When I click on the log out button
      Then should be redirected to the log in page

  Scenario: Normal user should be blocked from the manage panel
      Given I am a registered user
      And I am logged in
      When I visit the manage panel
      Then I should see the product listing for guests
