Feature: Premium Membership

  A user can purchase a premium membership in order to view discount prices

  Background: 
    Given I am a registered user
    And I am logged in

  Scenario: Discount price should not be visible to normal user
    Given I have populated the data for several products
    When I visit the webpage
    Then I should not see the discount price

  Scenario: Premium membership purchase link should be visible to regular user
    When I visit the webpage
    Then I should see the link to purchase premium membership

  Scenario: Premium purchase page
    When I visit the premium purchase page
    Then I should see the link to confirm my purchase

  Scenario: Purchase membership
    When I purchase the premium membership
    Then I should see my status updated to premium member

  Scenario: Discount price
    Given I am a premium member
    And I have populated the data for several products
    When I visit the webpage
    Then I should see the discount price

  Scenario: Product page for premium user
    Given I am a premium member
    And A burger is in the product listing
    When I am on the burger page
    Then I should see the product name for the burger
    And I should see the price for the burger
    And I should see the discount price for the burger

