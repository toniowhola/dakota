Feature: Product pictures

  Each product can have more than one picture.
  Admin can add pictures while adding the product.
  Admin can also add more pictures after the product is created.

  Background:
    Given I am an admin
    And I am logged in as admin

  Scenario: Add a product with a picture
    Given I am on the new product form page
    When I add a product with one picture
    Then I should see the picture in the product listing
    And I should see the picture in the product page

  Scenario: Add a product with more than one pictures
    Given I am on the new product form page
    When I add a product with two pictures
    Then I should see the picture in the product listing
    And I should see the two pictures in the product page

  Scenario: Add a product without a picture
    Given I am on the new product form page
    When I add a product without any picture
    Then I should see a placeholder picture in the product listing
    And I should see a placeholder picture in the product page

  Scenario: Add a picture to a product without picture
    Given I have a product with no picture
    When I add a picture to the product
    Then I should see the picture in the product listing
    And I should see the picture in the product page

  Scenario: Add a picture to a product with pictures
    Given I have a product with one picture
    When I add a picture to the product
    Then I should see the picture in the product listing
    And I should see the two pictures in the product page
