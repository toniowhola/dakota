Given(/^I am on the new product form page$/) do
  visit new_manage_product_path
end

When(/^I add a product with one picture$/) do
  fill_in "product_name", :with => "iMac G3"
  fill_in "product_price", :with => "30000"
  fill_in "product_discount_price", :with => "29999"
  attach_file "product_productimages_productpicture", Rails.root.join('features/support/images/g3.jpg')

  click_button "Create Product"
end

Then(/^I should see the picture in the product listing$/) do
  visit root_path

  expect(page).to have_css("img[src*='g3']")
end

Then(/^I should see the picture in the product page$/) do
  click_link "iMac G3"
  
  expect(page).to have_css("img[src*='g3.jpg']")
end

When(/^I add a product with two pictures$/) do
  fill_in "product_name", :with => "iMac G3"
  fill_in "product_price", :with => "30000"
  fill_in "product_discount_price", :with => "29999"
  attach_file "product_productimages_productpicture", 
    [
      Rails.root.join('features/support/images/g3_2.jpg'),
      Rails.root.join('features/support/images/g3.jpg')
    ]

  click_button "Create Product"
end

Then(/^I should see the two pictures in the product page$/) do
  click_link "iMac G3"

  expect(page).to have_css("img[src*='g3.jpg']")
  expect(page).to have_css("img[src*='g3_2.jpg']")
end

When(/^I add a product without any picture$/) do
  fill_in "product_name", :with => "iMac G3"
  fill_in "product_price", :with => "30000"
  fill_in "product_discount_price", :with => "29999"

  click_button "Create Product"
end

Then(/^I should see a placeholder picture in the product listing$/) do
  visit root_path

  expect(page).to have_xpath("//img[@src='/images/placeholder.jpg']")
end

Then(/^I should see a placeholder picture in the product page$/) do
  click_link "iMac G3"

  expect(page).to have_xpath("//img[@src='/images/placeholder.jpg']")
end

Given(/^I have a product with no picture$/) do
  @product = FactoryBot.create(:product,
                               :name => "iMac G3",
                               :price => 30000,
                               :discount_price => 29999)
end

When("I add a picture to the product") do
  visit edit_manage_product_path(@product)

  attach_file "productimage_productpicture", Rails.root.join('features/support/images/g3.jpg')

  click_button "Add now"
end

Given("I have a product with one picture") do
  @product = FactoryBot.create(:product,
                               :name => "iMac G3",
                               :price => 30000,
                               :discount_price => 29999)

  FactoryBot.create(
    :productimage,
    :product => @product,
    :productpicture => Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/g3_2.jpg'), 'image/jpeg')
  )
end
