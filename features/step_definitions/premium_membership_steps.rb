Then(/^I should see the link to purchase premium membership$/) do
  expect(page).to have_link("Get premium membership")
end

When(/^I visit the premium purchase page$/) do
  click_link "Get premium membership"
end

Then(/^I should see the link to confirm my purchase$/) do
  expect(page).to have_button("One-click purchase")
end

When(/^I purchase the premium membership$/) do
  visit new_membership_path
  
  click_button("One-click purchase")
end

Then(/^I should see my status updated to premium member$/) do
  visit root_path
  
  expect(page).to have_content("Membership: Premium")
end

Given(/^I am a premium member$/) do
  FactoryBot.create(:membership,
                    :user => @registered_user)
end

Then(/^I should see the discount price$/) do
  expect(page).to have_css('.discount-price')
  expect(page).to have_content("799")
end
