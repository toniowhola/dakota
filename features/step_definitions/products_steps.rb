Given(/^I have populated the data for several products$/) do
  FactoryBot.create(:product,
                    :name => "Robot",
                    :price => 800,
                    :discount_price => 799)
  FactoryBot.create(:product,
                    :name => "Rocket",
                    :price => 700,
                    :discount_price => 699)
  FactoryBot.create(:product,
                    :name => "Chair",
                    :price => 2000,
                    :discount_price => 1999)
end

Then(/^I should see the list of products$/) do
  expect(page).to have_content("Robot")
  expect(page).to have_content("Rocket")
  expect(page).to have_content("Chair")
end

Then(/^I should not see the discount price$/) do
  expect(page).to_not have_content("1999")
end

Given("A burger is in the product listing") do
  @burger = FactoryBot.create(:product, 
                               :name => "Burger",
                               :price => 2000,
                               :discount_price => 1999)
end

Then(/^I should not see the discount price for the burger$/) do
  expect(page).to_not have_content("1999")
end

When(/^I am on the burger page$/) do
  visit product_path(@burger)
end

Then(/^I should see the product name for the burger$/) do
  expect(page).to have_content("Burger")
end

Then(/^I should see the price for the burger$/) do
  expect(page).to have_content("2000")
end

Then(/^I should see the discount price for the burger$/) do
  expect(page).to have_content("1999")
end
