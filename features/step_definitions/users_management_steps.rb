Then(/^I should see a link to the user listing$/) do
  expect(page).to have_link("User listing")
end

Given(/^I have populated the data for several users$/) do
  FactoryBot.create(:user,
                    :email => "naomi@dakota.me",
                    :password => "u83E$$$$$")
  FactoryBot.create(:user,
                    :email => "grace@dakota.me",
                    :password => "u98B^^^^^")
end

When(/^I visit the user listing$/) do
  visit manage_users_path
end

Then(/^I should see the list of users$/) do
  expect(page).to have_content("naomi@dakota.me")
  expect(page).to have_content("grace@dakota.me")
end

Given(/^I have a user other than me in the listing$/) do
  @new_user = FactoryBot.create(:user,
                                :email => "nate@dakota.me",
                                :password => "silver_signal",
                                :admin => 0)
end

When(/^I set the user as admin$/) do
  visit manage_users_path

  find_by_id("toggle_admin_status_#{@new_user.id}").click
end

Then(/^the user should be able to see the manage panel after logging in$/) do
  visit root_path

  fill_in "user_email", :with => "nate@dakota.me"
  fill_in "user_password", :with => "silver_signal"  

  click_button "Log in"

  click_link "Manage panel"

  expect(page).to have_content("Product listing for admin")
end

Then("I should not Toggle admin") do
  expect(page).to_not have_link("Toggle admin")
end







