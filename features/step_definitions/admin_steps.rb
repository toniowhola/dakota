Given(/^I am an admin$/) do
  @admin_user = FactoryBot.create(:user,
                                  :email => "admin@dakota.me",
                                  :password => "meow^^",
                                  :admin => 1)
  FactoryBot.create(:membership,
                    :user => @admin_user)
end

Given(/^I am logged in as admin$/) do
  visit root_path

  fill_in "user_email", :with => "admin@dakota.me"
  fill_in "user_password", :with => "meow^^"

  click_button "Log in"
end

Then(/^I should see a link to the manage panel$/) do
  visit root_path 

  expect(page).to have_link("Manage panel")
end

When(/^I click on the manage panel link$/) do
  click_link("Manage panel")
end

Then(/^I should see the admin panel$/) do
  expect(page).to have_content("Product listing for admin")
end

When(/^I visit the manage panel$/) do
  visit manage_products_path
end

When(/^I submit a new product$/) do

  visit manage_products_path

  click_link "New Product"

  fill_in "product_name", :with => "Acetaminophen"
  fill_in "product_price", :with => "200"
  fill_in "product_discount_price", :with => "199"

  click_button "Create Product"
end

Then(/^I should see the new product in the product listing$/) do
  visit root_path

  expect(page).to have_content("Acetaminophen")
  expect(page).to have_content("200")
end

Then(/^I should see the new product in the admin product listing$/) do
  visit manage_products_path
  
  expect(page).to have_content("Acetaminophen")
  expect(page).to have_content("200")
  expect(page).to have_content("199")
end

Given(/^I have a product in my listing$/) do
  FactoryBot.create(:product, 
                    :name => "kindle",
                    :price => 3000,
                    :discount_price => 2999)
end

When(/^I change the name of the product$/) do
  visit manage_products_path

  click_link "Edit"

  fill_in "product_name", :with => "Kobo"

  click_button "Update Product"
end

Then(/^I should see the product with the new name in my listing$/) do
  visit root_path

  expect(page).to_not have_content("Kindle")
  expect(page).to have_content("Kobo")
end

When(/^I change the price of the product$/) do
  visit manage_products_path

  click_link "Edit"

  fill_in "product_price", :with => "4000"

  click_button "Update Product"
end

Then(/^I should see the product with the new price in my listing$/) do
  visit root_path

  expect(page).to_not have_content("3000")
  expect(page).to have_content("4000")
end

When(/^I change the discount price of the product$/) do
  visit manage_products_path

  click_link "Edit"

  fill_in "product_discount_price", :with => "3999"

  click_button "Update Product"
end

Then(/^I should see the product with the new discount price in my listing$/) do
  visit root_path

  expect(page).to_not have_content("2999")
  expect(page).to have_content("3999")
end

When(/^I remove a book from my inventory$/) do
  visit manage_products_path

  click_link "Destroy"
end

Then(/^I should not see it listed in the listing$/) do
  expect(page).to_not have_content("Kindle")
end
