Given("I visit the webpage") do
  visit root_path
end

When("I fill in the sign up form") do
  click_link "Sign up"
  
  fill_in "user_email", :with => "tony@dakota.me"
  fill_in "user_password", :with => "@_@wwwww"
  fill_in "user_password_confirmation", :with => "@_@wwwww"

  click_button "Sign up"
end

When("I confirm the email") do
  open_email("tony@dakota.me")
  visit_in_email("Confirm my account")
end

Then("I should see that my account is confirmed") do
  message = "Your email address has been successfully confirmed"
  expect(page).to have_content(message)
end

Given("I am a registered user") do
  @registered_user = FactoryBot.create(:user,
                                       :email => "tony@dakota.me",
                                       :password => "@_@wwwww")
end

When("I fill in the login form") do
  fill_in "user_email", :with => "tony@dakota.me"
  fill_in "user_password", :with => "@_@wwwww"

  click_button "Log in"
end

Then("I should be logged in") do
  expect(page).to have_content("Signed in successfully")
end

Given("I am logged in") do
  visit root_path

  fill_in "user_email", :with => "tony@dakota.me"
  fill_in "user_password", :with => "@_@wwwww"

  click_button "Log in"
end

When("I click on the log out button") do
  click_link "Log out"
end

Then("should be redirected to the log in page") do
  expect(page).to have_content("Log in")
end

Then("I should see the product listing for guests") do
  expect(page).to have_content("All products")
end
