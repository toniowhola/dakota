Feature: Users Management

  An admin user can view the list of users and set a user as admin

  Background: 
    Given I am an admin
    And I am logged in as admin

  Scenario: A link to user listing should be available to admin
    When I visit the manage panel
    Then I should see a link to the user listing
 
  Scenario: Listing users
    Given I have populated the data for several users
    When I visit the user listing
    Then I should see the list of users

  Scenario: Set a user as admin
    Given I have a user other than me in the listing
    When I set the user as admin
    And I click on the log out button
    Then the user should be able to see the manage panel after logging in

  Scenario: Current logged in admin cannot drop himself from admin
    When I visit the user listing
    Then I should not Toggle admin
