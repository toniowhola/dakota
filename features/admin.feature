Feature: Admin

  Only an admin user should be able to create/edit/delete products on this website

  Background:
    Given I am an admin
    And I am logged in as admin

  Scenario: Manage panel link should be available to admin
    When I visit the webpage
    Then I should see a link to the manage panel

  Scenario: Manage panel link should redirect the admin to the manage page
    When I click on the manage panel link
    Then I should see the admin panel

  Scenario: Listing products in the manage panel
    Given I have populated the data for several products
    When I visit the manage panel
    Then I should see the list of products
    
  Scenario: Adding a new product
    When I submit a new product
    Then I should see the new product in the admin product listing
    And I should see the new product in the product listing

  Scenario: Changing the name of a product
    Given I have a product in my listing
    When I change the name of the product
    Then I should see the product with the new name in my listing

  Scenario: Changing the price of a product
    Given I have a product in my listing
    When I change the price of the product
    Then I should see the product with the new price in my listing

  Scenario: Changing the discount price of a product
    Given I have a product in my listing
    When I change the discount price of the product
    Then I should see the product with the new discount price in my listing

  Scenario: Removing a product from my store
    Given I have a product in my listing
    When I remove a book from my inventory
    Then I should not see it listed in the listing
