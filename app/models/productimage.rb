class Productimage < ApplicationRecord
  validates :product_id, :productpicture, presence: true

  mount_uploader :productpicture, ProductpictureUploader

  belongs_to :product
end
