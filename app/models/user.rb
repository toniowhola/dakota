class User < ApplicationRecord

  validates_associated :membership

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  has_one :membership, :dependent => :destroy

  def is_admin
    return (self.admin == 1)
  end
  
  def admin_status_in_words
    if self.admin == 1
      return "Admin"
    else
      return "Normal user"
    end
  end

  def has_premium_membership
    return !self.membership.nil?
  end
end
