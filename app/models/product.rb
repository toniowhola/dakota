class Product < ApplicationRecord
  validates :name, :price, :discount_price, presence: true
  validates_associated :productimages
  validates :price, numericality: { only_integer: true }
  validates :discount_price, numericality: { only_integer: true }

  has_many :productimages, dependent: :destroy

  def cover_image
    if !self.productimages.exists?
      return "/images/placeholder.jpg"
    else
      return self.productimages.order("created_at ASC").first.productpicture_url(:square)
    end
  end
end
