class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  def admin_only
    redirect_to root_path if !current_user.is_admin
  end
end
