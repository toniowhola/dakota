class Manage::ProductimagesController < ApplicationController
  def create
    @productimage = Productimage.new(productimage_params)
    @product = @productimage.product
    
    if @productimage.save
      redirect_to edit_manage_product_path(@product), notice: "A new picture was successfully added."
    else
      redirect_to edit_manage_product_path(@product), alert: "Failed to add a photo. Please try again later."
    end
  end

  def destroy
    @productimage = Productimage.find(params[:id])
    @product = @productimage.product
    @productimage.destroy
    respond_to do |format|
      format.html { redirect_to edit_manage_product_path(@product), notice: 'Image was successfully destroyed.' }
      format.json { head :no_content }
    end

  end

  private
    def productimage_params
      params.require(:productimage).permit(:product_id, :productpicture)
    end
end
