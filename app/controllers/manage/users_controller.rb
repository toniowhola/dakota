class Manage::UsersController < ApplicationController
  before_action :admin_only

  def index
    @users = User.all
    @highlight_option_users = 'selected'
  end

  def toggle_admin
    user = User.find(params[:id])
    if user.admin == 0
      user.update_attribute(:admin, 1)
    else
      user.update_attribute(:admin, 0)
    end
    redirect_to manage_users_path
  end

end
