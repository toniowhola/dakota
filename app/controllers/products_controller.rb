class ProductsController < ApplicationController

  def index
    @products = Product.all
    @show_discount = current_user.has_premium_membership
  end

  def show
    @product = Product.find(params[:id])
    @show_discount = current_user.has_premium_membership
  end
end
