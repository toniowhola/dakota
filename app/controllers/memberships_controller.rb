class MembershipsController < ApplicationController
  def new
    @membership = Membership.new
  end

  def create
    @membership = Membership.find_or_initialize_by(user_id: current_user.id)

    respond_to do |format|
      if @membership.save
        format.html { redirect_to root_path, notice: 'You have purchased the premium membership successfully' }
      else
        format.html { render :new }
      end
    end
  end
end
