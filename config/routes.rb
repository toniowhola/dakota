Rails.application.routes.draw do
  devise_for :users

  root to: "products#index"

  resources :products, :memberships
  namespace :manage do
    resources :products, :productimages
    resources :users do
      member do
        get 'toggle_admin'
      end
    end
  end
end
