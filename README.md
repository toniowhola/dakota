# dakota

### yet another membership system

## Main features
* A user can register as a normal user
* A user can browse products on this website
* A user can choose to purchase premium membership(can skip payment)
* A premium member can see products with discount prices
* A user can be set as admin
* An admin can create/edit products on this website
* A product would have product price/name/image(s)


## Rails / Ruby version
Rails 5.2.2   
Ruby 2.6.0


## Used Gems
We're using the following gems.   
devise => for user authentication   
carrierwave => for image upload   
rspec => for writing unit tests   
cucumber => for writing acceptance tests   
shoulda-matchers => for model tests   
factorybot => fixtures   
database clean => set up clean env between test runs   
email_spec => test confirmation email from devise   
bootstrap => view framework   
unicorn => http server   
rmagick => image processing   


## Testing
Acceptance test is done through Cucumber   
To run the test suites, you can do   

	> $bundle exec cucumber

Unit test is done through RSpec   
To run the test suites, you can do    

	> $bundle exec rspec

## Deployment
This application is deployed to aws with unicorn and nginx while we have postgresql as the production database.   
You can find a testable website at http://3.94.98.222   

An admin account was setup as seed for bootstrap.   
id: admin@dakota.me   
pwd: testadmin
   
A Gmail account was setup to send the confirmation email when a new user signs up.   
Email account: dakotabuybuybuy@gmail.com   
*Make sure you also check your junk mail folder if you don't get the confirmation email.   



