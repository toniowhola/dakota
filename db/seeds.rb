# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin_user = User.new(
  email: "admin@dakota.me",
  password: "testadmin",
  password_confirmation: "testadmin",
  admin: 1
)
admin_user.skip_confirmation!
admin_user.save!

user_a = User.new(
  email: "justin@dakota.me",
  password: "aaaaaaaa",
  password_confirmation: "aaaaaaaa"
)
user_a.skip_confirmation!
user_a.save!

user_b = User.new(
  email: "sandy@dakota.me",
  password: "bbbbbbbb",
  password_confirmation: "bbbbbbbb"
)
user_b.skip_confirmation!
user_b.save!

Membership.create(user_id: user_b.id)

product_a = Product.create(
  name: "iMac",
  price: 1999,
  discount_price: 999)
product_a.productimages.create(productpicture: Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/g3_2.jpg'), 'image/jpeg'))

product_b = Product.create(
  name: "Chair",
  price: 8792,
  discount_price: 6346)
product_b.productimages.create(productpicture: Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/chair.jpg'), 'image/jpeg'))

product_c = Product.create(
  name: "Fountain Pen",
  price: 1399,
  discount_price: 599)
product_c.productimages.create(productpicture: Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/fountain_pen.jpg'), 'image/jpeg'))

product_d = Product.create(
  name: "Beer",
  price: 59,
  discount_price: 35)
product_d.productimages.create(productpicture: Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/beer.png'), 'image/png'))

product_e = Product.create(
  name: "Macbook Pro",
  price: 78900,
  discount_price: 68000)
product_e.productimages.create(productpicture: Rack::Test::UploadedFile.new(Rails.root.join('features/support/images/mbp.jpg'), 'image/jpeg'))

