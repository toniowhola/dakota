class CreateProductimages < ActiveRecord::Migration[5.2]
  def change
    create_table :productimages do |t|
      t.string :productpicture
      t.belongs_to :product, foreign_key: true

      t.timestamps
    end
  end
end
