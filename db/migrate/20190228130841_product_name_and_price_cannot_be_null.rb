class ProductNameAndPriceCannotBeNull < ActiveRecord::Migration[5.2]
  def change
    change_column :products, :name, :string, :null => false
    change_column :products, :price, :integer, :null => false
  end
end
