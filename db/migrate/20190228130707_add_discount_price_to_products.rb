class AddDiscountPriceToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :discount_price, :integer, null: false
  end
end
